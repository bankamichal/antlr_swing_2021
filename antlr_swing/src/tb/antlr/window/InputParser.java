package tb.antlr.window;

import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.tree.CommonTree;

import tb.antlr.ExprLexer;
import tb.antlr.ExprParser;

public class InputParser {
	
	private final CommonTokenStream tokens;
	
	public InputParser(String input) {
		ANTLRStringStream stream = new ANTLRStringStream(input + "\n");
		ExprLexer lexer = new ExprLexer(stream);
		this.tokens = new CommonTokenStream(lexer);
	}
	
	public CommonTokenStream getTokens() {
		return this.tokens;
	}
	
	public CommonTree parse() {
		ExprParser parser = new ExprParser(tokens);
	    ExprParser.start_return root = null;
	    
		try {
			root = parser.start();
		} catch (RecognitionException e) {
			e.printStackTrace();
		}

		return root.getTree();
	}
}
