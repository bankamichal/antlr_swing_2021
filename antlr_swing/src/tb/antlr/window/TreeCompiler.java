package tb.antlr.window;

import java.io.FileReader;
import java.io.IOException;

import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.tree.CommonTreeNodeStream;
import org.antlr.stringtemplate.StringTemplate;
import org.antlr.stringtemplate.StringTemplateGroup;

import tb.antlr.kompilator.TExpr3;

public class TreeCompiler {
	
	public StringTemplate parseTreeCompile(CommonTreeNodeStream nodes) {
		
		String packagePath = TExpr3.class.getPackage().getName().replaceAll("\\.", "/");
		
		FileReader groupFile;
		StringTemplateGroup templates = null;
		try {
			groupFile = new FileReader("src/" + packagePath + "/pierwszy.stg");
			templates = new StringTemplateGroup(groupFile);
			groupFile.close();
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}

		TExpr3 walker = new TExpr3(nodes);							// Tworzymy parser drzew korzystający z powyższego bufora
		walker.setTemplateLib(templates);							// Tłumaczymy parserowi drzew, jakich szablonów ma używać
		
		TExpr3.compile_return tpl = null; 								// Wywołujemy parser drzew - startując od reguły prog (Tym razem z klasy TExpr3!)
		try {
			tpl = walker.compile();
		} catch (RecognitionException e) {
			e.printStackTrace();
		}

		return (StringTemplate) tpl.getTemplate();    // Pobierz "wypełniony" szablon
		
	}
	
}
