package tb.antlr.window;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.tree.TreeNodeStream;

import tb.antlr.interpreter.TExpr1;

public class TreeInterpreter {

	public ByteArrayOutputStream parseTreeInterpret(TreeNodeStream nodes) {
		TExpr1 interpreter = new TExpr1(nodes); 							    // Tworzymy parser drzew korzystający z powyższego bufora
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(baos);
		System.setOut(ps);

		try {																	// Wywołujemy parser drzew - startując od reguły prog (Tym razem z klasy TExpr1!)
			interpreter.interpret();
		} catch (RecognitionException e) {
			e.printStackTrace();
		}
		
		return baos;
	}
	
}
