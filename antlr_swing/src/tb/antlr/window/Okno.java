package tb.antlr.window;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.antlr.runtime.tree.CommonTreeNodeStream;
import org.antlr.stringtemplate.StringTemplate;
import org.antlr.runtime.tree.CommonTree;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class Okno extends JFrame {

	private JPanel contentPane;

	private JButton testButton;
	
	private JTextPane inputPane;
	private JScrollPane inputPaneScrollable;
	private JButton parseButton;
	
	private JTextPane astPane;
	private JScrollPane astPaneScrollable;
	private JButton interpretButton;

	private JTextPane resultPane;
	private JScrollPane resultPaneScrollable;
	private JButton compileButton;
	
	private JTextPane assemblyPane;
	private JScrollPane assemblyPaneScrollable;

	private CommonTreeNodeStream nodes;

	public Okno() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 700);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		testButton = new JButton("Default input");				//przycisk dodający domyślne wartości, stworzony do szybszego testowania manualnego
		testButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//czytanie z pliku input.txt
				try {
					Path path = Paths.get("./src/tb/antlr/input.txt");
					List<String> lines = Files.lines(path).collect(Collectors.toList());
					
					String inputText = String.join("\n", lines);
					inputPane.setText(inputText);
					
				} catch(IOException e) {
					System.err.println(e);
				}
			}
		});
		testButton.setBounds(5, 5, 280, 23);
		contentPane.add(testButton);
		
		inputPane = new JTextPane();
		inputPane.setBounds(5, 35, 280, 250);
		inputPane.getDocument().addDocumentListener(new DocumentListener() {
			
			@Override
			public void removeUpdate(DocumentEvent e) {
				modifyWindowElements(true);
			}
			
			@Override
			public void insertUpdate(DocumentEvent e) {
				modifyWindowElements(true);
			}
			
			@Override
			public void changedUpdate(DocumentEvent e) {
				modifyWindowElements(true);
				
			}
		});
		inputPaneScrollable = new JScrollPane(inputPane,
	            JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
	            JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		inputPaneScrollable.setBounds(inputPane.getBounds());
		contentPane.add(inputPaneScrollable);

		parseButton = new JButton("Parse");
		parseButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				parseInput();
			}
		});
		parseButton.setBounds(295, 5, 150, 23);
		contentPane.add(parseButton);

		astPane = new JTextPane();
		astPane.setEditable(false);
		astPane.setBounds(455, 5, 280, 280);
		astPaneScrollable = new JScrollPane(astPane,
	            JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
	            JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		astPaneScrollable.setBounds(astPane.getBounds());
		
		contentPane.add(astPaneScrollable);

		interpretButton = new JButton("Interpretuj (TExpr1)");
		interpretButton.setEnabled(false);
		interpretButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				parseTreeInterpret();
			}
		});
		interpretButton.setBounds(5, 348, 280, 23);
		contentPane.add(interpretButton);

		resultPane = new JTextPane();
		resultPane.setEditable(false);
		resultPane.setBounds(5, 382, 280, 280);
		resultPaneScrollable = new JScrollPane(resultPane,
	            JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
	            JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		resultPaneScrollable.setBounds(resultPane.getBounds());
		
		contentPane.add(resultPaneScrollable);

		compileButton = new JButton("Kompiluj(TExpr3)");
		compileButton.setEnabled(false);
		compileButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				parseTreeCompile();
			}
		});
		compileButton.setBounds(455, 348, 280, 23);
		contentPane.add(compileButton);

		assemblyPane = new JTextPane();
		assemblyPane.setEditable(false);
		assemblyPane.setBounds(455, 382, 280, 280);
		assemblyPaneScrollable = new JScrollPane(assemblyPane, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		assemblyPaneScrollable.setBounds(assemblyPane.getBounds());
		
		contentPane.add(assemblyPaneScrollable);
	}
	
	protected void modifyWindowElements(boolean isTreeInvalid) {
		interpretButton.setEnabled(!isTreeInvalid);
		compileButton.setEnabled(!isTreeInvalid);
		astPane.setForeground(isTreeInvalid ? Color.LIGHT_GRAY : Color.BLACK);
	}

	private void parseInput() {
		String input = inputPane.getText();
		System.out.println(input);
		InputParser parser = new InputParser(input);
		CommonTree result = parser.parse(); 
		if (result != null) {
			astPane.setText(result.toStringTree());
			nodes = new CommonTreeNodeStream(result);
			nodes.setTokenStream(parser.getTokens());
			modifyWindowElements(false);
		}
	}
	
	private void parseTreeCompile() {
		TreeCompiler treeCompiler = new TreeCompiler();
		StringTemplate template = treeCompiler.parseTreeCompile(nodes);
		if (template != null) {
			assemblyPane.setText(template.toString());
			nodes.reset();
		}
	}

	private void parseTreeInterpret() {
		PrintStream old = System.out;
		TreeInterpreter treeInterpreter = new TreeInterpreter();
		ByteArrayOutputStream stream = treeInterpreter.parseTreeInterpret(nodes);
		
		resultPane.setText(stream.toString());
		System.setOut(old);
		nodes.reset();
	}
}
