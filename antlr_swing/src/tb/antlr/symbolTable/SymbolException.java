package tb.antlr.symbolTable;

public class SymbolException extends RuntimeException {
	
	public SymbolException(String message) {
		super(message);
	}
}
