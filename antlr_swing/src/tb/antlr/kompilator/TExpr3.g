tree grammar TExpr3;

options {
  tokenVocab=Expr;
  ASTLabelType=CommonTree;
  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer depth = 0;
}

// dzięki takiej budowie prog każda linia inputu jest w oknie kompilatora oddzielona pustą linią
compile  : (d+=decl | e+=expr | e+=block)* -> template(name={$e},deklaracje={$d})
"<deklaracje;separator=\"\n\n\">

start:
<name;separator=\"\n\n\"> "
;


block
  : (LB  {enterBlock();depth++;} -> block_in())
  | (RB  {leaveBlock();depth--;} -> block_out())
  ;


decl  : ^(VAR i1=ID) {initializeVariable(getVarName($ID.text, depth));} -> declare_var(name={getVarName($ID.text, depth)}); 
          catch [RuntimeException ex] {errorID(ex,$i1);}


expr returns [Integer out]    
        : ^(PLUS  e1=expr e2=expr) {$out = $e1.out + $e2.out;} -> add(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr) {$out = $e1.out - $e2.out;} -> sub(p1={$e1.st},p2={$e2.st})
        | ^(MUL   e1=expr e2=expr) {$out = $e1.out * $e2.out;} -> mul(p1={$e1.st},p2={$e2.st})
        | ^(DIV   e1=expr e2=expr) {$out = $e1.out / $e2.out;} -> div(p1={$e1.st},p2={$e2.st})
        | ^(PODST id=ID   e1=expr) {setVariable(getVarName($id.text, depth), $e1.out);} -> set_var(name={getVarName($id.text,depth)}, value={$e1.st})
        |   INT                    {$out = getInt($INT.text);} -> mov_int_to_reg(value={$INT.text})
        |   ID                     {$out = getVariable(getVarName($ID.text, depth));} -> get_var(name={getVarName($ID.text, depth)}, value={getVariable(getVarName($ID.text, depth))})
    ;
    