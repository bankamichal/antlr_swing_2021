/**
 * 
 */
package tb.antlr.kompilator;

import org.antlr.runtime.RecognizerSharedState;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.TreeNodeStream;
import org.antlr.runtime.tree.TreeParser;

import tb.antlr.symbolTable.GlobalSymbols;
import tb.antlr.symbolTable.LocalSymbols;
import tb.antlr.symbolTable.SymbolException;

public class TreeParserTmpl extends TreeParser {
	
	private final LocalSymbols localSymbols;
	
	
	public TreeParserTmpl(TreeNodeStream input) {
		super(input);
		this.localSymbols = new LocalSymbols();
	}

	public TreeParserTmpl(TreeNodeStream input, RecognizerSharedState state) {
		super(input, state);
		this.localSymbols = new LocalSymbols();
	}

	protected void errorID(RuntimeException ex, CommonTree id) {
		System.err.println(ex.getMessage() + " in line " + id.getLine());
	}
	
	protected String getVarName(String var, Integer depth) {
		return var + "_" + depth.toString();
	}
	
	protected void initializeVariable(String name) {
		if (localSymbols.hasSymbol(name)) {
			throw new SymbolException("Symbol '" + name + "' already exists");
		}
		localSymbols.addSymbol(name);
	}
	
	protected void setVariable(String name, Integer value) {
		if (!localSymbols.hasSymbol(name)) {
			throw new SymbolException("Symbol '" + name + "' does not exist");
		}
		localSymbols.setSymbol(name, value);
	}
	
	protected Integer getVariable(String name) {
		if (!localSymbols.hasSymbol(name)) {
			throw new SymbolException("Symbol '" + name + "' does not exist");
		}
		if (localSymbols.getSymbol(name) == null) {
			throw new SymbolException("Symbol '" + name + "' has no value");
		}
		return localSymbols.getSymbol(name);
	}
	
	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
	protected void enterBlock() {
		localSymbols.enterScope();
	}
	
	protected void leaveBlock() {
		localSymbols.leaveScope();
	}

}
