package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;

import tb.antlr.symbolTable.LocalSymbols;
import tb.antlr.symbolTable.SymbolException;

public class MyTreeParser extends TreeParser {

	private final LocalSymbols localSymbols;
	
    public MyTreeParser(TreeNodeStream input) {
        super(input);
        this.localSymbols = new LocalSymbols();
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
        this.localSymbols = new LocalSymbols();
    }

    protected void print(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
	protected void initializeVariable(String name) {
		if (localSymbols.hasSymbol(name)) {
			throw new SymbolException("Symbol '" + name + "' already exists");
		}
		localSymbols.addSymbol(name);
	}
	
	protected void setVariable(String name, Integer value) {
		if (!localSymbols.hasSymbol(name)) {
			throw new SymbolException("Symbol '" + name + "' does not exist");
		}
		localSymbols.setSymbol(name, value);
	}
	
	protected Integer getVariable(String name) {
		if (!localSymbols.hasSymbol(name)) {
			throw new SymbolException("Symbol '" + name + "' does not exist");
		}
		if (localSymbols.getSymbol(name) == null) {
			throw new SymbolException("Symbol '" + name + "' has no value");
		}
		return localSymbols.getSymbol(name);
	}
	
	protected void enterBlock() {
		localSymbols.enterScope();
	}
	
	protected void leaveBlock() {
		localSymbols.enterScope();
	}
}
