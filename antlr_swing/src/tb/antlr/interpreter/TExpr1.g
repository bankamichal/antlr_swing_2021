tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
}

//interpret    : (e=expr {print($e.text + " = " + $e.out.toString());} )* ;

interpret : (expr | print | block)*;

print : ^(PRINT e=expr) {print($e.text + " = " + $e.out.toString());};

block
  : LB  {enterBlock(); }
  | RB  {leaveBlock(); }
  ;

expr returns [Integer out]
    : ^(PLUS  e1=expr e2=expr)  {$out = $e1.out + $e2.out;}
    | ^(MINUS e1=expr e2=expr)  {$out = $e1.out - $e2.out;}
    | ^(MUL   e1=expr e2=expr)  {$out = $e1.out * $e2.out;}
    | ^(DIV   e1=expr e2=expr)  {$out = $e1.out / $e2.out;}
    | ^(POW   e1=expr e2=expr)  {$out = (int)Math.pow($e1.out, $e2.out);}
    | ^(VAR   id=ID)            {initializeVariable($id.text);}
    | ^(PODST id=ID   e1=expr)  {setVariable($id.text, $e1.out);}
    |   INT                     {$out = getInt($INT.text);}
    |   ID                      {$out = getVariable($ID.text);}
    ;
