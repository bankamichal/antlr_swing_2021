grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

start
    : (stat | block)+ EOF!;
    
    
block
    : LB (stat)* RB
    ;

stat
    : expression1 NL -> expression1
    | VAR ID PODST expression1 NL -> ^(VAR ID) ^(PODST ID expression1)
    | VAR ID NL -> ^(VAR ID)
    | ID PODST expression1 NL -> ^(PODST ID expression1)
    | PRINT expression1 NL -> ^(PRINT expression1)
    | NL ->
    ;

expression1
    : expression2
      ( PLUS^ expression2
      | MINUS^ expression2
      )*
    ;

expression2
    : expression3
      ( MUL^ expression3
      | DIV^ expression3
      )*
    ;

expression3
    : atom
      (POW^ atom)?
    ;

atom
    : INT
    | ID
    | LP! expression1 RP!
    ;


VAR :'var';

PRINT
  : 'print'
  ;

ID : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;

INT : '-'? '0'..'9'+;
 
NL : '\r'? '\n' ;

WS : (' ' | '\t')+ {$channel = HIDDEN;} ;

LP
	:	'('
	;

RP
	:	')'
	;

LB
  : '{'
  ;
  
RB
  : '}'
  ;  
  
PODST
	:	'='
	;

PLUS
	:	'+'
	;

MINUS
	:	'-'
	;

MUL
	:	'*'
	;

DIV
	:	'/'
	;

	
POW
  : '^'
  ;
